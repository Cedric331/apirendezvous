<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evenement extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titre', 'nom', 'lieu',
    ];


    protected $hidden = ['id', 'created_at', 'updated_at'];

    public function creneaux()
    {
        return $this->hasMany(Creneaux::class);
    }

    public function vote()
    {
        return $this->hasMany(Vote::class);
    }
}
