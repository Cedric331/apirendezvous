<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Creneaux extends Model
{
    /**
* The attributes that are mass assignable.
*
* @var array
*/
protected $fillable = [
'date',
];

protected $table = 'creneaux';

protected $hidden = ['id', 'created_at', 'updated_at', 'evenement_id'];
}
