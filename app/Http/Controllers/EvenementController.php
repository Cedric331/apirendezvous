<?php

namespace App\Http\Controllers;

use App\Creneaux;
use App\Evenement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EvenementController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'titre' => 'bail|required|string|max:255',
            'nom' => 'bail|required|string|max:100',
            'lieu' => 'bail|required|string|max:255',
            'date.*' => 'bail|required|date_format:Y/m/d|after:today'
        ]);


        $errors = $validator->errors();

        if ($validator->fails()) {
                return response()->json($errors, 401);
        }

        $hashId = hash('sha256', uniqid());
        $hashAdmin = hash('sha256', uniqid());

        $event = new Evenement;

        $event->titre = $request->titre;
        $event->nom = $request->nom;
        $event->lieu = $request->lieu;
        $event->hashId = $hashId;
        $event->hashAdmin = $hashAdmin;
        $event->save();

        $evenement = Evenement::firstWhere('hashId', $hashId);
        $id =  $evenement->id;

        foreach ($request->date as $date) {
        $creneaux = new Creneaux;

        $creneaux->date = $date;
        $creneaux->evenement_id = $id;
        $creneaux->save();
        }

        foreach ($event->creneaux as $key => $value)
        {
            $event->creneaux[$key] = $value->date;
        }

        return response()->json($event, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Evenement  $evenement
     * @return \Illuminate\Http\Response
     */
    public function show($hash)
    {
        $evenement = Evenement::firstWhere('hashId', $hash);

        $id =  $evenement->id;

        $event = Evenement::findOrFail($id);
        $event->makeHidden(['hashAdmin']);

        foreach ($event->creneaux as $key => $value)
        {
            $event->creneaux[$key] = $value->date;
        }

        $event->vote;
        return response()->json($event, 200);
    }

        /**
     * Display the specified resource.
     *
     * @param  \App\Evenement  $evenement
     * @return \Illuminate\Http\Response
     */
    public function showAdmin($hash)
    {
        $evenement = Evenement::firstWhere('hashAdmin', $hash);

        $id =  $evenement->id;

        $event = Evenement::findOrFail($id);
        $event->makeHidden(['hashAdmin']);

        foreach ($event->creneaux as $key => $value)
        {
            $event->creneaux[$key] = $value->date;
        }

        $event->vote;
        return response()->json($event, 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Evenement  $evenement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $hash)
    {
        $request->validate([
            'titre' => 'bail|required|string|max:255',
            'nom' => 'bail|required|string|max:100',
            'lieu' => 'bail|required|string|max:255',
        ]);

        $evenement = Evenement::firstWhere('hashAdmin', $hash);

        $id =  $evenement->id;

        $requete = Evenement::findOrFail($id);
        $requete->update($request->all());

        return response()->json($requete, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Evenement  $evenement
     * @return \Illuminate\Http\Response
     */
    public function destroy($hash)
    {
        $evenement = Evenement::firstWhere('hashAdmin', $hash);

        $id =  $evenement->id;

        $requete = Evenement::findOrFail($id);
        $requete->delete();

        return response()->json("Evènement supprimé", 204);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Evenement  $evenement
     * @return \Illuminate\Http\Response
     */
    public function showDateAdmin($hash)
    {
        $evenement = Evenement::firstWhere('hashAdmin', $hash);

        $id =  $evenement->id;

        $event = Evenement::findOrFail($id);
        $event->makeHidden(['hashAdmin', 'hashId', 'titre', 'lieu', 'nom']);
        $event->creneaux;

        foreach ($event->creneaux as $key => $value)
        {
            $event->creneaux[$key] = $value->date;
        }

        return response()->json($event, 200);
    }

        /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Evenement  $evenement
     * @return \Illuminate\Http\Response
     */
    public function storeDate(Request $request, $hash)
    {
        $validator = Validator::make($request->all(),[
            'date.*' => 'bail|required|date_format:Y/m/d|after:today'
        ]);


        $errors = $validator->errors();

        if ($validator->fails()) {
                return response()->json($errors, 401);
        }

        $evenement = Evenement::firstWhere('hashAdmin', $hash);
        $id =  $evenement->id;

        foreach ($request->date as $date) {
        $creneaux = new Creneaux;

        $creneaux->date = $date;
        $creneaux->evenement_id = $id;
        $creneaux->save();
        }

        $retour = $evenement->creneaux;

        return response()->json($retour, 201);
    }
}
