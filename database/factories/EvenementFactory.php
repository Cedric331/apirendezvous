<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Creneaux;
use App\Evenement;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Evenement::class, function (Faker $faker) {
    return [
        'hashId' => $faker->password,
        'titre' => $faker->lastName,
        'nom' => $faker->lastName,
        'lieu' => $faker->city,
        'hashAdmin' => $faker->password,
    ];
});

$factory->define(Creneau::class, function (Faker $faker) {
    return [
        'date' => $faker->date,
        'evenement_id' => 5
    ];
});

$factory->define(Vote::class, function (Faker $faker) {
    return [
        'vote' => "1",
        'nom' => $faker->lastName,
        'evenement_id' => 5
    ];
});
