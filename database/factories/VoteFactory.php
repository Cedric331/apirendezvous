<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Creneaux;
use App\Evenement;
use App\Vote;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/



$factory->define(Vote::class, function (Faker $faker) {
    return [
        'vote' => "1",
        'nom' => $faker->lastName,
        'evenement_id' => 5
    ];
});
