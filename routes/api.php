<?php

use App\Http\Controllers\EvenementController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Récupère l'évènement via l'id
Route::get('/evenement/{hash}', 'EvenementController@show')->name('evenement');

// Publier un évènement
Route::post('/evenement', 'EvenementController@store')->name('evenement.creer');

// Ajouter un vote
Route::post('/evenement/{hash}/vote', 'VoteController@store')->name('evenement.vote');


// ADMIN

// Afficher l"évènement de l'admin
Route::get('/evenement/admin/{hashAdmin}', 'EvenementController@showAdmin')->name('evenement.affiche');

// Modifier un évènement
Route::patch('/evenement/admin/{hashAdmin}', 'EvenementController@update')->name('evenement.modifier');

// Supprime un évènement
Route::delete('/evenement/admin/{hashAdmin}', 'EvenementController@destroy')->name('evenement.delete');

// DATE

// Afficher les dates de l'évènement de l'admin
Route::get('/evenement/admin/{hashAdmin}/date', 'EvenementController@showDateAdmin')->name('evenement.date');

// Modifier un évènement
Route::post('/evenement/admin/{hashAdmin}/date', 'EvenementController@storeDate');
